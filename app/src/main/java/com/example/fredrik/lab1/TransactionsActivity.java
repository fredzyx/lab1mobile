package com.example.fredrik.lab1;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class TransactionsActivity extends AppCompatActivity
{
    String newListItem;
    ListView list;

    ArrayAdapter<String> arrayAdapter;

    ArrayList<String> listItems=new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        list = findViewById(R.id.listView);

        arrayAdapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listItems );
        list.setAdapter(arrayAdapter);

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3)
        {
            if(resultCode == RESULT_OK)
            {
                newListItem= data.getStringExtra("TRANSACTION");
                listItems.add(newListItem);

            }
        }


    }
}
