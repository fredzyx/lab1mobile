package com.example.fredrik.lab1;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TransferActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
    EditText amountView;
    TextView balanceView;
    String amountText;
    float amountValue;
    float balanceValue;
    String balanceFloat;
    Button payBtn;
    TextView amountCheck;
    Spinner friendList;
    boolean nameSelected;
    String name;
    TextView recipient;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        payBtn = findViewById(R.id.btn_pay);
        amountCheck = findViewById(R.id.lbl_amount_check);
        nameSelected = false;
        friendList = (Spinner) findViewById(R.id.friendList);
        friendList.setOnItemSelectedListener(this);
        recipient = findViewById(R.id.recipientTextView);
        balanceView = findViewById(R.id.balanceView);
        //amountValue = 1;
        amountValue = 0f;

        Spinner dropdown = findViewById(R.id.friendList);
        String[] friends = new String[]{"Jan", "Erlend", "Tuva", "Håvard", "Bram", "Bjørnar", "Morten"};
        //hvordan det skal vises
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, friends);
        dropdown.setAdapter(adapter);

        amountView = findViewById(R.id.txt_amount);


        float balance = getIntent().getExtras().getFloat("BALANCE");
        balanceFloat = String.format(Locale.US,"%.2f", balance);
        balanceValue = Float.parseFloat(balanceFloat);
        balanceView.setText(""+balanceValue);


        Toast.makeText(this, ""+ balanceValue, Toast.LENGTH_SHORT).show();


        amountView.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                //amountCheck.setVisibility(View.INVISIBLE);
                amountConfirm();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

                amountConfirm();



            }

            @Override
            public void afterTextChanged(Editable s)
            {
                amountConfirm();
                if(amountView.getText().toString().isEmpty())
                {
                    amountCheck.setVisibility(View.INVISIBLE);
                }


            }
        });
    }

    public void amountConfirm()
    {
        if(!amountView.getText().toString().isEmpty())
        {
            amountText = amountView.getText().toString();
            amountValue = Float.parseFloat(amountText);
            if (amountValid())
            {
                payBtn.setClickable(true);
                amountCheck.setVisibility(View.INVISIBLE);

            } else if (!amountValid())
            {
                payBtn.setClickable(false);
                //out of bounds
                amountCheck.setVisibility(View.VISIBLE);
            }
        }
    }

    public boolean amountValid()
    {
        if (amountValue <= 0f || amountValue > balanceValue|| !nameSelected)
        {
            return false;
        } else
        {
            return true;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        name = parent.getItemAtPosition(position).toString();
        recipient.setText(name);
        nameSelected = true;


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {
        nameSelected = false;
    }

    public void payButton(View view)
    {

        balanceValue = balanceValue - amountValue;
        balanceFloat = String.format(Locale.US,"%.2f", balanceValue);
        balanceValue = Float.parseFloat(balanceFloat);
        balanceView.setText("" + balanceValue);
        amountConfirm();

        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh.mm.ss aa");
        String time = dateFormat.format(currentTime);


        Intent intent = new Intent(TransferActivity.this, MainActivity.class)  ;

        intent.putExtra("BALANCE", balanceFloat);
        intent.putExtra("NAME",name);
        intent.putExtra("TIME", time);
        startActivityForResult(intent,2);
        setResult(RESULT_OK, intent);
        finish();
    }

}