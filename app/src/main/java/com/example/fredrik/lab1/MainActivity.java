package com.example.fredrik.lab1;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity
{


    TextView balanceView;

    String balanceValue;
    String newBalance;
    float randNum;
    boolean clicked = false;
    String name;
    String time;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        balanceView = findViewById(R.id.lbl_balance);

        randNum = new Random().nextFloat() * ((110F-90F))+90F;
        if (clicked)
        {
            //float balance = getIntent().getExtras().getFloat("BALANCE");
            //balanceView.setText(""+newBalance+" EUR");
            balanceValue = newBalance;
        }
        else
        {
            balanceValue = String.format(Locale.US,"%.2f", randNum);
            balanceView.setText(""+balanceValue+" EUR");

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1)
        {
            if(resultCode == RESULT_OK)
            {
                newBalance = data.getStringExtra("BALANCE");
                balanceView.setText(""+newBalance+" EUR");
            }
        }else if(requestCode == 2)
        {
            if(resultCode == RESULT_OK)
            {
                name = data.getStringExtra("NAME");
                time = data.getStringExtra("TIME");

            }

        }
    }



    public void transferButton(View view)
    { clicked = true;
      Intent i = new Intent(MainActivity.this, TransferActivity.class)  ;
      //String extraMessage = balanceValue;
      i.putExtra("BALANCE", randNum);
      startActivityForResult(i,1);


    }


    public void transactionsButton(View view)
    {
        String transactionData = time + "  |  " + name + "  |  " + balanceValue + "  |  " + newBalance;
        Intent tIntent = new Intent(MainActivity.this, TransactionsActivity.class);
        tIntent.putExtra("DATA", transactionData);
        startActivityForResult(tIntent,2);
    }
}
